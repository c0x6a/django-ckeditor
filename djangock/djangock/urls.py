"""djangock URL Configuration
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
"""
from django.urls import include, path

urlpatterns = [
    path('', include('apps.blog.urls', namespace='blog')),
]
