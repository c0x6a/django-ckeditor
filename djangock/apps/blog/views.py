from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from apps.blog.forms import PostEntryModelForm
from apps.blog.models import PostEntry


class PostEntryListView(ListView):
    template_name = 'index.html'
    model = PostEntry
    paginate_by = 20


class PostEntryDetailView(DetailView):
    template_name = 'view_post.html'
    model = PostEntry
    slug_url_kwarg = 'slug'
    slug_field = 'slug'


class PostEntryCreateView(CreateView):
    template_name = 'create_edit.html'
    form_class = PostEntryModelForm
    success_url = reverse_lazy('blog:index')


class PostEntryUpdateView(UpdateView):
    template_name = 'create_edit.html'
    form_class = PostEntryModelForm
    model = PostEntry
    slug_url_kwarg = 'slug'
    slug_field = 'slug'

    def get_success_url(self):
        success_url = reverse('blog:detail', args=[self.object.slug])
        return success_url
