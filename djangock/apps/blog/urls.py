from django.urls import path

from . import views

app_name = 'blog'

urlpatterns = [
    path(
        '',
        views.PostEntryListView.as_view(),
        name='index'
    ),
    path(
        'new/',
        views.PostEntryCreateView.as_view(),
        name='create'
    ),
    path(
        'read/<str:slug>/',
        views.PostEntryDetailView.as_view(),
        name='detail'
    ),
    path(
        'update/<str:slug>/',
        views.PostEntryUpdateView.as_view(),
        name='update'
    ),
]
