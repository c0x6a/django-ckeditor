from django.db import models
from django.template.defaultfilters import slugify


class PostEntry(models.Model):
    title = models.CharField(max_length=128)
    text = models.TextField(default='This is a new post')
    slug = models.SlugField()

    def __str__(self):
        return self.title

    def save(self, **kwargs):
        self.slug = slugify(self.title[:50])
        super().save(**kwargs)
