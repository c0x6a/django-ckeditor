from django import forms

from apps.blog.models import PostEntry


class PostEntryModelForm(forms.ModelForm):
    class Meta:
        model = PostEntry
        fields = (
            'title',
            'text',
        )
