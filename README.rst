Django with CKEditor
====================

This is a little and dumb project to show integration of `CKEditor
<https://ckeditor.com/>`_ with `Django
<https://www.djangoproject.com/>`_ as someone in a `Telegram Django
group <https://t.me/django/38629>`_ wanted a little bit of help on how
to integrate it in a project.


How to install and run yourself:
--------------------------------

Simply create a virtual environment and install dependencies:

.. code-block:: shell

   $ pip install -r requirements.txt

Set an environment variable called ``DJANGO_SECRET_KEY`` to, guess what,
save the django ``SECRET_KEY``, then run migrations and run the server
(move to the ``djangock`` folder first).

.. code-block:: shell

   $ ./manage.py migrate
   $ ./manage.py runserver

then just open a browser and go to `http://127.0.0.1:8000/
<http://127.0.0.1:8000/>`_.
